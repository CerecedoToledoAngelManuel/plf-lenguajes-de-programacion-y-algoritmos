# 1.-Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml 
@startmindmap 
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .orange {
    BackgroundColor #f33501
  }
}
</style>
* Programando ordenadores \nen los 80 y ahora 
 ** Sistemas Antiguos <<green>>
 ***_ son: <<orange>>
 **** Aquellos ordenadores de los años 80, 90 \nque ya no tienen continuidad o ya no estan a la venta <<blue>>
 *****_ velocidad es: <<orange>>
 ****** Medida en Megahercio <<rose>>
 *******_ se buscaba: <<orange>>
 ******** Cada maquina nueva \nfuera distinta <<rose>>
 *********_ se tenia: <<orange>>
 ********** Menos potencia y \nlimitacion de recursos <<rose>>
 ***********_ programas y juegos: <<orange>>
 ************ Estaban terminados y no \npresentaban fallas <<rose>>
 *************_ actualmente: <<orange>>
 ************** Funcionan igual que \nel primer dia <<rose>>
 *****_ su programacion: <<orange>>
 ****** Buscaba aprovechar los \npocos recursos <<rose>>
 *******_ para: <<orange>>
 ******** Realizar programas eficientes \na la hora de ejecutarse <<rose>>
 *********_ utilizando: <<orange>>
 ********** Un lenguaje que la \nmaquina entienda <<rose>>
 ***********_ como: <<orange>>
 ************ Ensamblador <<rose>>
 *************_ ventaja: <<orange>>
 ************** Un control total \nde la ejecucion <<rose>>
 ***************_ respecto: <<orange>>
 **************** Tiempo <<rose>>
 ***************** Ubicacion de \nlas Variables <<rose>>
 *************_ desventaja: <<orange>>
 ************** Programacion en \nmaquinas distintas <<rose>>
 ***************_ ya que: <<orange>>
 **************** Se tenia que conocer a \nfondo la arquitectura <<rose>>
 ** Sistemas Actuales <<green>>
 ***_ son: <<orange>>
 **** Aquellos ordenadores con los \nque contamos en casa o \npodemos comprar <<blue>>
 *****_ velocidad es: <<orange>>
 ****** Medidad en Gigahercios <<rose>>
 *******_ evoluciono en: <<orange>>
 ******** Memorias mucho mas \npotentes <<rose>>
 ******** Incremento de potencia <<rose>>
 ******** Disponibilidad de \nrecursos <<rose>>
 *******_ programas y juegos: <<orange>>
 ******** Presentan fallas <<rose>>
 ******** Se actualizan constantemente <<rose>>
 ******** Presencia de parches \nque ocasionan ineficiencia <<rose>>
 *******_ actualmente: <<orange>>
 ******** Las maquinas con el paso \ndel tiempo seran \nmas lentas <<rose>>
 *****_ su programacion: <<orange>>
 ****** Se preocupa por la \ninterfaz en lugar del \ntrabajo util <<rose>>
 *******_ para: <<orange>>
 ******** Realizar programas que \nsean multiplataforma <<rose>>
 *********_ utilizando: <<orange>>
 ********** Un lenguaje que \ndemuestre como piensa \nun programador <<rose>>
 ***********_  como: <<orange>>
 ************ Un lenguaje de alto nivel <<rose>>
 *************_ ventajas: <<orange>>
 ************** El programador ya no \ntiene que conocer \ncada plataforma <<rose>>
 ************** Permiten la programacion \nporcapas,creando\n niveles de abstraccion <<rose>>
 *************_ desventajas: <<orange>>
 ************** La sobrecarga debido a la \ngran cantidad de capas, \nafecta el tiempo de la GPU <<rose>>
 ************** Se generan cuellos de \nbotella  <<rose>>
@endmindmap
```

# 2.-Historia de los algoritmos y de los lenguajes de programación (2010)
```plantuml 
@startmindmap 
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .orange {
    BackgroundColor #f33501
  }
}
</style>
 * Historia de los Algoritmos y \nlenguajes de programación
 ** Algoritmos <<green>>
 ***_ son:
 **** Secuencia ordenada y \nfinita de pasos <<blue>>
 *****_ utilizados para:
 ****** Mostrar la manera de \nllevar a cabo un \nproceso <<rose>>
 ****** Resolver problemas \nmatematicos <<rose>>
 *****_ su evolucion inicio:
 ****** Antigua \nMesopotamia <<rose>>
 *******_ con:
 ******** Transacciones \nComerciales <<rose>>
 *********_ posteriormente:
 ********** Siglo XVLL <<rose>>
 ***********_ donde surgieron:
 ************ Las primeras \ncalculadoras de \nbolsillo <<rose>>
 *************_ teniendo auge en el: 
 ************** Siglo XX \n con Maquinas Programables <<rose>>
 *****_ divididos en:
 ****** Razonables o \n polinomiales <<rose>>
 *******_ consiste en:
 ******** Algoritmos con tiempo \nde ejecucion despacio \na medida que los problemas se \nhacen mas grandes <<rose>>
 ****** No razonables, \nexponenciales o \nno super polinomiales <<rose>>
 *******_ se refiere:
 ******** Duplicacion del \ntiempo, creciendo muy deprisa \nocasionando que los algoritmos \nno puedan resolver problemas muy grandes <<rose>>
 ** Lenguajes de programacion <<green>>
 ***_ son:
 **** Instrumentos que nos permiten \nejecutar algoritmos <<blue>>
 *****_ para crear:
 ****** Programas <<rose>>
 *******_ que son:
 ******** Conjunto de \noperaciones especificadas \nen un lenguaje de \nprogramacion <<rose>>
 *****_ implementa: 
 ****** Paradigmas <<rose>>
 *******_ los cuales:
 ******** Tienen una perspectiva de \nconcebir los computos <<rose>>
 *********_ se dividen en:
 ********** Imperativo <<orange>>
 ***********_ consiste en:
 ************ Secuencia de \nordenes a la maquina <<rose>>
 *************_ ejemplo:
 ************** Fontran en 1954 <<rose>>
 ********** Funcional <<orange>>
 ***********_ consiste en:
 ************ Simplificacion de \nlas expresiones <<rose>>
 *************_ ejemplo:
 ************** Lisp en 1960 <<rose>> 
 ********** Orientado a Objetos <<orange>>
 ***********_ considerada como:
 ************ Celulas que se \ncomunican entre si \na traves de mensajes <<rose>>
 *************_ ejemplo:
 ************** Simula en 1968 <<rose>>
 ********** Logicos <<orange>>
 ***********_ utilizado en:
 ************ Problemas de \noptimizacion e \ninteligencia artificial <<rose>>
 *************_ ejemplo:
 ************** Prolog en 1971 <<rose>>
@endmindmap 
```

# 3.-Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml 
@startmindmap
<style>
mindmapDiagram {
  .rose {
      BackgroundColor pink
  }
  .blue {
      BackgroundColor SkyBlue
  }
  .red {
      BackgroundColor Tomato
  }
  .green {
    BackgroundColor lightgreen
  }
}
</style>
 * Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. \nLa evolución de los lenguajes y paradigmas de programación
 **_ lenguaje de programacion tiene:
 *** Adaptarse a la crecientes \nnecesidades <<rose>>
 ****_ para:
 ***** Resolver problemas diversos \ny mas complejos <<rose>>
 ******_ y asi:
 ******* Ofrecer eficiencia en la \nejecucion <<rose>>
 **_ un:
 *** Paradigma <<blue>>
 ****_ es una 
 ***** Forma de aproximarse a la solucion de un problema <<blue>>
 ******_ o bien:
 ******* Manera en que se concibe la \nprogramacion para una \nsoluccion concreta <<blue>>
 ********_ tipos de paradigmas:
 ********* Estructurado <<red>>
 ********** Estuctura de \ndatos abstractos <<blue>>
 *********** Estructuras de \ncontrol que \npermiten \ncontrolar el flujo \ndel programa <<blue>>
 ************ C \nPascal \nFortan <<blue>>
 ********* Funcional <<red>>
 ********** Basado en \nFormulas o \nteoremas \nmatematicos <<blue>>
 *********** Recursividad <<blue>>
 ************ ML \nHaskell <<blue>>
 ********* Logico <<red>>
 ********** Basado en \nexpresiones \nlogicas <<blue>>
 *********** Regresa \ncierto o falso <<blue>>
 ************ Prolog <<blue>>
 ********* Distribuida <<red>>
 ********** Un programa \npuede estar en \ndiversos ordenadores \nejecutandose <<blue>>
 *********** Erlang <<blue>>
 ********* Orientada a \nobjetos <<red>>
 ********** Crea objetos \nbasados en \nelementos de \nsoftware <<blue>>
 *********** Busca establecer \nun dialogo entre \nobjetos <<blue>>
 ************ Java <<blue>>
 ********* Basada en \ncomponentes <<red>>
 ********** Conjunto de objetos \ncon mucho mas nivel \nde abstraccion <<blue>>
 *********** Permite reutilizar \nlos componentes en \nnuevos programas <<blue>>
 ********* Orientao a \naspectos <<red>>
 ********** Consiste en programar \nlos aspectos por separado <<blue>>
 *********** Al final del \nproyecto busca integrarlos <<blue>>
 ************ Ayuda a que el codigo \n sea mas entendible <<blue>>
 ********* Orientada a \nagentes <<red>>
 ********** Se basa en \nagentes denominandose \nentidades autonomas <<blue>>
 *********** Perciben su \nentorno para \nalcanzar sus objetivos <<blue>>
 ************ Utilizado en proyectos \nde inteligencia artificial <<blue>>
 **_ tiene como:
 *** Tendencias <<green>>
 **** Encontrar paradigmas  \nmas abstractos <<green>>
 **** Encontrar paragidmas \n mas cercanos a nuestra \nforma natural de expresarnos <<green>>
@endmindmap 
```
# AUTOR: "Cerecedo Toledo Angel Manuel"










